from telegram.ext import Updater
import requests
import lxml.html


def dilbert(event, context):
    updater = Updater(token='Your Telegram bot token')

    response = requests.get("http://dilbert.com/")
    doc = lxml.html.document_fromstring(response.text)
    element = doc \
        .xpath('//*[@id="js_comics"]/div/div[1]/div/section/div[2]/a/img')[0]
    keys = element.keys()
    values = element.values()
    dictionary = dict(zip(keys, values))
    image_url = str(dictionary["src"])

    # Put here the chat id of your telegram bot
    chat_id = 12345678
    updater.bot.send_photo(chat_id=chat_id, photo="http:" + image_url)


def fut_data(event, context):
    import boto3
    import fut
    from fut import extras
    from datetime import datetime
    from operator import itemgetter

    startTime = datetime.now()
    client = boto3.resource('dynamodb', region_name='us-east-1')
    table = client.Table("fut_prices")
    players = fut.core.players().values()
    filtered_players = list(filter(lambda d: d['rating'] >= 88, players))
    filtered_players = sorted(filtered_players, key=itemgetter('rating'))
    date = str(datetime.now())
    print("Saving {} players".format(len(filtered_players)))
    for player in filtered_players:
        price = extras.futheadPrice(player['id'], platform='ps')
        row_id = date + "-" + str(player['id'])
        data = {
            "row_id": row_id,
            "id": player['id'],
            "firstname": player["firstname"],
            "lastname": player["lastname"],
            "surname": player["surname"],
            "price": price,
            "source": "futhead",
            "rating": player['rating'],
            "platform": "ps",
            "date": date
        }
        table.put_item(Item=data)
    print("Round finished in {}".format(datetime.now() - startTime))
